import os
import cv2
import time
import math
import numpy
import numpy as np
import imageio
import label_map_util
#import queue, threading
import tensorflow as tf
import xml.etree.ElementTree as ET
from collections import defaultdict
import random
#import threading
import warnings
#from utils import visualization_utils as vis_util
from logging.handlers import TimedRotatingFileHandler
warnings.filterwarnings("ignore")
import visualization_utils as vis_util

def box_normal_to_pixel(box, dim):
    
    """Function is used to convert absolute(pixel) cordinate into normalized cordinates.
    
    Args:box are the 4 absolue(pixel) cordinates(x1,y1,x2,y2) of detected objects 
         dim is the dimension(eg:720*680) of image (image shape)
    
    Function returns numpy array of original cordiantes. 
    """    
    height, width = dim[0], dim[1]
    box_pixel = [int(box[0]*height), int(box[1]*width), int(box[2]*height), int(box[3]*width)]
    return np.array(box_pixel)
       
def box_Center(loc):
    """Function finds centroid of detected box.
     
    Args: loc is normalized cordinate of detected object at perticular frame.
    
    return : center of box 
    """
    height=int(loc[3]-loc[1])
    width= int(loc[2]-loc[0])
    center=((loc[0]+(width/2)),(loc[1]+(height/2)))
    return center
    

def display(image_np):

   
    cv2.imshow("image",image_np)
    cv2.waitKey()
    
def myfunction():
    full_image=[]

    
    #s_time = time.time()                    
    image_np = cv2.imread("2000.jpg")
    #print("image",image_np)
    
    #image_np = image_np[y1_crop:y2_crop,x1_crop:x2_crop]
    image_np = cv2.resize(image_np, (416,416))
    dim = image_np.shape[0:2]
            
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    scores = detection_graph.get_tensor_by_name('detection_scores:0')
    classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    # Actual detection.
    (boxes, scores, classes, num_detections) = sess.run(
        [boxes, scores, classes, num_detections],
        feed_dict={image_tensor: image_np_expanded})
    vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=8)
    
            
    boxes=np.squeeze(boxes)
    classes =np.squeeze(classes)
    scores = np.squeeze(scores)
                
    cls = classes.tolist()
    print("cls",cls)
                                                      
    dets = []
    values=[]
    veh_class=[]
    dets1=[]
    for i, value in enumerate(cls):
        
        if scores[i] >.5:
            if int(value)==1:
          
                                  
                (y1, x1) = (boxes[i][0], boxes[i][1])
                (y2, x2) = (boxes[i][2], boxes[i][3])
                dets.append([x1,y1,x2,y2])
                veh_class.append(int(value))
        if scores[i]>.5:
            if int(value)==2:
                (b1, a1) = (boxes[i][0], boxes[i][1])
                (b2, a2) = (boxes[i][2], boxes[i][3])
                dets1.append([a1,b1,a2,b2])
                veh_class.append(int(value))
                
            
            
        
    print("veh_class",veh_class)
  
                                                
    dets = np.asarray(dets)
    print("dets",dets)
    dets1 = np.asarray(dets1)
    if  len(dets)!= 0 :
                        
        for i in range(len(dets)):
             nor_cord=box_normal_to_pixel(dets[i],dim)
             height=int(nor_cord[3]-nor_cord[1])
             width= int(nor_cord[2]-nor_cord[0])
             front_point= [int(nor_cord[0]+(width)),int(nor_cord[1]+height)] # Here front of lower line is conside
             center=[int(nor_cord[0]+(width/2)),int(nor_cord[1]+height)]  # Here center of lower line is conside
             #end_point= [int(nor_cord[0]),int(nor_cord[1]+height)]       # Here end of lower line is conside
             #veh_id.append(veh_class[i])
             total_area=height*width
             print("veh_class",veh_class[i])
             full_image.append(image_np)
    if len(dets)!=0:
        for i in range(len(dets1)):
             nor_cord=box_normal_to_pixel(dets1[i],dim)
             height=int(nor_cord[3]-nor_cord[1])
             width= int(nor_cord[2]-nor_cord[0])
             front_point= [int(nor_cord[0]+(width)),int(nor_cord[1]+height)] # Here front of lower line is conside
             center=[int(nor_cord[0]+(width/2)),int(nor_cord[1]+height)]  # Here center of lower line is conside
             #end_point= [int(nor_cord[0]),int(nor_cord[1]+height)]       # Here end of lower line is conside
             #veh_id.append(veh_class[i])
             damage_area=height*width
             
             full_image.append(image_np)
            
    
        
    f_damage_area=(damage_area/total_area)*100
    cv2.putText(image_np, "scratch_area"+"-"+str(f_damage_area), (20,600), cv2.FONT_HERSHEY_SIMPLEX , 2,  
                 (0, 255, 0) , 2, cv2.LINE_AA, False) 
        
    display(image_np)
                 
   
           
if __name__ == '__main__':
    PATH_TO_FROZEN_GRAPH = 'frozen_inference_graph.pb'
    # List of the strings that is used to add correct label for each box.
    PATH_TO_LABELS = 'AVCC.pbtxt'
    try:
       
        
        detection_graph = tf.Graph()
        with detection_graph.as_default():   
            od_graph_def = tf.compat.v1.GraphDef()
            with tf.io.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid: 
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
                print("model succes fulyy loaded")
        category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)
        
        # config file to use gpu memories 
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = .1
    except Exception as e:
        print("error",e)
    try:
        with detection_graph.as_default():                    
            with tf.Session(graph=detection_graph) as sess:
                myfunction()
    except Exception as e:
        print(e)
                
        print("maiun function is failing")               
               
                    
else:
           
    print("image_capture_fail")
       
   # video_capture.stop()
    cv2.destroyAllWindows()
       
